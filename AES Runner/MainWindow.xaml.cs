﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AES_Runner
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        // S-Box 부분 코드 출처: http://snipplr.com/view/67929/

        // Precomputed Rijndael S-BOX
        int[] _sbox = { 0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe,
            0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72,
            0xc0, 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, 0x04,
            0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83, 0x2c,
            0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed, 0x20,
            0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33,
            0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc,
            0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e,
            0x3d, 0x64, 0x5d, 0x19, 0x73, 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde,
            0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4,
            0x79, 0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, 0xba,
            0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 0x70, 0x3e, 0xb5,
            0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11, 0x69,
            0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42,
            0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

        // Precomputed inverted Rijndael S-BOX
        int[] _rsbox = { 0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81,
            0xf3, 0xd7, 0xfb, 0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9,
            0xcb, 0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e, 0x08,
            0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25, 0x72, 0xf8, 0xf6,
            0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, 0x6c, 0x70, 0x48, 0x50, 0xfd,
            0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84, 0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3,
            0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06, 0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1,
            0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b, 0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf,
            0xce, 0xf0, 0xb4, 0xe6, 0x73, 0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c,
            0x75, 0xdf, 0x6e, 0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe,
            0x1b, 0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4, 0x1f,
            0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f, 0x60, 0x51, 0x7f,
            0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, 0xa0, 0xe0, 0x3b, 0x4d, 0xae,
            0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61, 0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6,
            0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d };
        //


        public MainWindow()
        {
            InitializeComponent();
            sBoxRijndael_Checked(null, null);
            key1_Checked(null, null);
        }
        ulong TEST = 0;

        int[] SBox = new int[256]; // 바이트 -> 바이트
        int[] ISBox = new int[256];

        int[][] plaintext =
            { new int[] { 85, 85, 85, 85 }, new int[] { 85, 85, 85, 85 }
            , new int[] { 85, 85, 85, 85 }, new int[] { 85, 85, 85, 85 } };

        int[][] desiredCiphertext =
            { new int[] { 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0 }
            , new int[] { 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0 } };

        int[][] state;

        int[][] round0Key;
        int[][] round1Key =
            { new int[] { 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0 }
            , new int[] { 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0 } };
        int[] Rcon1 = { 1, 0, 0, 0 };

        private void button_Click(object sender, RoutedEventArgs e)
        {


            Init();

            AddRound0Key();
            SubBytes();
            ShiftRows();
            AddRound1Key();

            resultLabel.Content = "";

            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    string h = state[i][j].ToString("X2");
                    //h = h.Substring(h.Length - 2, 2);
                    resultLabel.Content += h;
                }

        }

        void Init()
        {
            for (int i = 0; i < 4; i++)
            {
                int[] temp = (int[])(i == 0 ? round0Key[3] : round1Key[i - 1]).Clone();
                if (i == 0)
                {
                    int temp2 = temp[0];
                    for (int t = 0; t < 3; t++)
                        temp[t] = temp[t + 1];
                    temp[3] = temp2;

                    for (int t = 0; t < 4; t++)
                    {
                        temp[t] = SBox[temp[t]] ^ Rcon1[t];
                    }
                }

                for (int j = 0; j < 4; j++)
                    round1Key[i][j] = round0Key[i][j] ^ temp[j];
            }

            state = new int[][] { (int[])plaintext[0].Clone(), (int[])plaintext[1].Clone(),
                (int[])plaintext[2].Clone(), (int[])plaintext[3].Clone()};
        }

        void SubBytes()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    state[i][j] = SBox[state[i][j]];
                }
            }
        }

        void ShiftRows()
        {
            int temp = state[1][0];
            state[1][0] = state[1][1];
            state[1][1] = state[1][2];
            state[1][2] = state[1][3];
            state[1][3] = temp;

            temp = state[2][0];
            state[2][0] = state[2][2];
            state[2][2] = temp;
            temp = state[2][1];
            state[2][1] = state[2][3];
            state[2][3] = temp;

            temp = state[3][0];
            state[3][0] = state[3][3];
            state[3][3] = state[3][2];
            state[3][2] = state[3][1];
            state[3][1] = temp;
        }

        void MixColumn()
        {
            throw new NotImplementedException();
        }

        void AddRound0Key()
        {
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    state[i][j] ^= round0Key[i][j];
                }
        }

        void AddRound1Key()
        {
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    state[i][j] ^= round1Key[i][j];
                }
        }

        private void testButton_Click(object sender, RoutedEventArgs e)
        {
            /*
            List<Tuple<int, int>> AnsList = new List<Tuple<int, int>>();

            for (int i = 0; i < 255; i++)
            {
                for (int j = 0; j < 255; j++)
                {
                    if ((SBox[0x55 ^ i] ^ SBox[j] ^ i) == 0)
                        AnsList.Add(new Tuple<int, int>(i, j));
                }
            }

            List<Tuple<int, int>> AnsList2 = new List<Tuple<int, int>>();

            for (int i = 0; i < 255; i++)
            {
                AnsList2.Add(new Tuple<int, int>(i, ISBox[SBox[0x55 ^ i] ^ i]));
            }*/

            DateTime LastNow = DateTime.Now;

            int k0, k1, k2, k3;

            for (k0 = 0; k0 < 255; k0++)
            {
                Console.WriteLine("K0: " + k0);
                Console.WriteLine(DateTime.Now);

                for (k1 = 0; k1 < 255; k1++)
                {

                    for (k2 = 0; k2 < 255; k2++)
                    {
                        for (k3 = 0; k3 < 255; k3++)
                        {
                            int k13 = ISBox[SBox[0x55 ^ k0] ^ k0 ^ 1];
                            int k14 = ISBox[SBox[0x55 ^ k1] ^ k1];
                            int k15 = ISBox[SBox[0x55 ^ k2] ^ k2];
                            int k12 = ISBox[SBox[0x55 ^ k3] ^ k3];

                            int k0_4_8 = k12 ^ 1 ^ SBox[0x55 ^ k15] ^ SBox[k13];
                            int k2_6_10 = k14 ^ SBox[0x55 ^ k13] ^ SBox[k15];
                            int k1_5_9 = k13 ^ SBox[0x55 ^ k12] ^ SBox[k14];
                            int k3_7_11 = k15 ^ SBox[0x55 ^ k14] ^ SBox[k12];


                            int k8 = 0x55 ^ ISBox[SBox[k15] ^ k2_6_10];
                            int k11 = 0x55 ^ ISBox[SBox[k14] ^ k1_5_9];
                            int k10 = 0x55 ^ ISBox[SBox[k13] ^ k0_4_8 ^ 1];
                            int k9 = 0x55 ^ ISBox[SBox[k12] ^ k3_7_11];

                            int k2_6 = k2_6_10 ^ k10;
                            int k1_5 = k1_5_9 ^ k9;
                            int k0_4 = k0_4_8 ^ k8;
                            int k3_7 = k3_7_11 ^ k11;

                            int k6 = 0x55 ^ ISBox[SBox[k14] ^ k1_5];
                            int k5 = 0x55 ^ ISBox[SBox[k13] ^ k0_4 ^ 1];
                            int k4 = 0x55 ^ ISBox[SBox[k12] ^ k3_7];
                            int k7 = 0x55 ^ ISBox[SBox[k15] ^ k2_6];

                            if (k0 == (k0_4 ^ k4) && k1 == (k1_5 ^ k5) && k2 == (k2_6 ^ k6) && k3 == (k3_7 ^ k7))
                            {
                                string message = String.Format("K0: {0}, K1: {1}, K2: {2}, K3: {3}, K4: {4}, K5: {5}, " +
                                    "K6: {6}, K7: {7}, K8: {8}, K9: {9}, K10: {10}, K11: {11}, K12: {12}, K13: {13}, " +
                                    "K14: {14}, K15: {15}", k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13, k14, k15);
                                Console.WriteLine(message);
                                if (MessageBox.Show(message,
                                    "Result - Press yes to keep finding", MessageBoxButton.YesNo) == MessageBoxResult.No)
                                {
                                    return;
                                    
                                }                                
                            }
                        }
                    }
                }
            }

        }

        private void key2_Checked(object sender, RoutedEventArgs e)
        {
            round0Key = new int[][]
                { new int[] { 167, 8, 76, 105 }, new int[] { 56, 3, 199, 215 }
                , new int[] { 25, 26, 58, 184 }, new int[] { 17, 78, 134, 226 } };
        }

        private void key1_Checked(object sender, RoutedEventArgs e)
        {
            round0Key = new int[][]
                { new int[] { 0, 0, 0, 0 }, new int[] { 1, 1, 1, 1 }
                , new int[] { 1, 0, 0, 1 }, new int[] { 85, 84, 85, 85 } };
                
        }

        private void key3_Checked(object sender, RoutedEventArgs e)
        {
            round0Key = new int[][]
                { new int[] { 167, 101, 94, 115 }, new int[] { 147, 22, 108, 67 }
                , new int[] { 87, 254, 48, 214 }, new int[] { 79, 78, 216, 63 } };
        }

        private void sBoxRijndael_Checked(object sender, RoutedEventArgs e)
        {
            SBox = (int[])_sbox.Clone();
            ISBox = (int[])_rsbox.Clone();
        }

        private void sBoxIdentity_Checked(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 256; i++)
                SBox[i] = ISBox[i] = i;
        }

        
    }
}
